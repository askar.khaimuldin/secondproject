package week3.Task1.domain;

import java.util.HashSet;

public class Programmer extends Employee {
    private String language;

    public Programmer(String name, String surname, int salary, String language) {
        super(name, surname, salary);
        setLanguage(language);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
