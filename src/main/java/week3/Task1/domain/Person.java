package week3.Task1.domain;

public class Person {
    private int id;
    private static int id_gen = 0;
    private String name;
    private String surname;

    public Person(String name, String surname) {
        generateId();
        setName(name);
        setSurname(surname);
    }

    private void generateId() {
        id = id_gen++;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Override
    public String toString() {
        return id + " " + name + " " + surname;
    }
}
