package week3.Task1;

import week3.Task1.domain.Employee;
import week3.Task1.domain.Musician;
import week3.Task1.domain.Person;
import week3.Task1.domain.Programmer;

public class Main {
    public static void main(String[] args) {
        Person employee = new Employee("John", "Watson", 30000);
        Person programmer = new Programmer("Steve", "Jobs", 1000000, "Java");
        // Person  ->   Employee   ->   Programmer
        Person singer = new Musician("John", "Lennon", "guitar");
        // Casting -* when you convert one data type to another

        System.out.println(employee);
        System.out.println(programmer);
    }
}
